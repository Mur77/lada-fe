import express from 'express'
import next from 'next'
import { createProxyMiddleware } from 'http-proxy-middleware'

const port = process.env.PORT || 3000

const dev = process.env.NODE_ENV !== 'production'

const app = next({ dev })

const handle = app.getRequestHandler()

const apiPaths = {
    '/api': {
        target: 'https://dev.api.com',

        pathRewrite: {
            '^/api': '/api',
        },

        changeOrigin: true,
    },
}

const isDevelopment = process.env.NODE_ENV !== 'production'

//app.use(express.static('public'))

app.prepare()
    .then(() => {
        const server = express()

        if (isDevelopment) {
            server.use('/api', createProxyMiddleware(apiPaths['/api']))
        }

        server.all('*', (req, res) => {
            return handle(req, res)
        })

        server.listen(port, (err) => {
            if (err) throw err

            console.log(`> Ready on http://localhost:${port}`)
        })
    })
    .catch((err) => {
        console.log('Error:::::', err)
    })
