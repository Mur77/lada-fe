export const messages = {
    header: {
        logoText: 'Бахмутское общество защиты животных "Лада"',
        menu: {
            main: 'Главная',
            about: 'О нас',
            lookHome: 'Ищем дом',
            ourAnimals: 'Наши подопечные',
            howToHelp: 'Как нам помочь',
            ourFriends: 'Наши друзья',
            contacts: 'Контакты',
        },
        submenu2: {
            pos1: 'Команда',
            pos2: 'Проекты',
            pos3: 'Отчетность',
        },
        submenu3: {
            pos1: 'Собачкам',
            pos2: 'Котикам',
        },
        submenu4: {
            pos1: 'Под опекой',
            pos2: 'Счастливые истории',
        },
        submenu5: {
            pos1: 'Стать частью команды',
            pos2: 'Помочь финансово',
            pos3: 'Помочь физически, материально',
            pos4: 'Стать куратором животного',
            pos5: 'Взять животное на передержку',
        },
    },
    home: {
        dogsLooking: 'Собаки ищут хозяев',
        catsLooking: 'Кошки ищут хозяев',
        looking: 'Ищу хозяина!',
        ladaNumbers: 'ЛАДА в цифрах',
        weHave: 'Сейчас у нас:',
        foundHome: 'Нашли дом:',
        more: 'Подробнее',
    },
    footer: {
        about: '© 2021 Бахмутское общество защиты животных "Лада"',
        ourFacebook: 'Мы в Facebook',
        ourInstagram: 'Мы в Instagram',
    },
    team: {
        title: 'Наша команда',
    },
}