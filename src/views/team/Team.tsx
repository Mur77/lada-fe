import { Img } from '../../components'

import { iUser } from '../../interfaces'
import { messages } from '../../messages/messages'

import styles from './Team.module.scss'

import { teamMembers as members } from './teamMembers'

interface iTeam {
    teamMembers?: iUser[]
}

export const Team = ({ teamMembers = members }) => {
    return (
        <div className={styles.container}>
            <div className={styles.title}>{messages.team.title}</div>
            <div className={styles.teamContainer}>
                {teamMembers.map((item) => (
                    <div className={styles.memberContainer} key={item.id}>
                        <Img
                            src={item.image}
                            alt={item.description}
                            placeholderPath='/images/placeholder/user-placeholder.png'
                            className={styles.image}
                        />
                        <div className={styles.caption}>
                            <div>{item.name}</div>
                            <div>{item.position}</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}