import { Slider } from '../../components'
import { Intro } from './components/intro/Intro'
import { WeekAnimal } from './components/weekAnimal/WeekAnimal'
import { AnimalTotals } from './components/animalTotals/AnimalTotals'
import { LookingHost } from './components/lookingHost/LookingHost'
import { HowToHelp } from './components/howToHelp/HowToHelp'
import { OurFriends } from './components/ourFriends/OurFriends'

import { messages } from '../../messages/messages'

import styles from './Home.module.scss'

import AllCats from '../../../public/icons/cat1.svg'
import AllDogs from '../../../public/icons/dog1.svg'

import { slider1, sliderCats } from './data/slides'
import { intro } from './data/intro'
import { weekAnimals } from './data/week'
import { friends } from './data/friends'

export const Home: React.FC = () => {
    return (
        <div className={styles.container}>

            <div className={styles.section1}>
                <div className={styles.section1SliderContainer}>
                    <Slider slides={slider1} backgroundColor='none' />
                </div>
                <div className={styles.section1RightSide}>
                    <Intro text={intro.text} />
                </div>
            </div>

            <div className={styles.section2}>
                <div className={styles.leftSide}>
                    <div className={styles.section1RightSideTitle}>
                        {messages.home.ladaNumbers}
                    </div>
                    <AnimalTotals
                        title={messages.home.weHave}
                        rightIcon={AllDogs}
                        leftIcon={AllCats}
                        rightAmount={256}
                        leftAmount={128}
                    />
                    <AnimalTotals
                        title={messages.home.foundHome}
                        rightIcon={AllDogs}
                        leftIcon={AllCats}
                        rightAmount={256}
                        leftAmount={128}
                    />
                </div>
                <div className={styles.rightSide}>
                    <WeekAnimal {...weekAnimals[0]} />
                    <WeekAnimal {...weekAnimals[1]} />
                </div>
            </div>

            <div className={styles.section3}>
                <LookingHost animal={sliderCats[0]} />
            </div>

            <div className={styles.section4}>
                <HowToHelp />
            </div>

            <div className={styles.section5}>
                <OurFriends friends={friends} />
            </div>

        </div>
    )
}