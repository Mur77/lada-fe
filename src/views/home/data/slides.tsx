import { iHomeSlide, iAnimal } from '../../../interfaces'

export const slider1: iHomeSlide[] = [
    {
        id: '0',
        image: 'images/pictures/img2497-1200.jpg',
        text: <div>Huisissl</div>,
        description: 'Slide1',
    },
    {
        id: '1',
        image: 'images/pictures/img2500-1200.jpg',
        text: <div>Myaso</div>,
        description: 'Slide2',
    },
    {
        id: '2',
        image: 'images/pictures/img2593-1200.jpg',
        text: <div>Kisi</div>,
        description: 'Slide3',
    },
]

export const sliderCats: iAnimal[] = [
    {
        id: '0',
        image: 'images/pictures/cat-slider-1.webp',
        nickname: 'Кася',
        description: 'Кася - бело-рыжая кошка с грустными глазами.',
        quote: 'Буду самым верным другом. Никому не расскажу, как ты ешь по ночам, буду есть вместе с тобой :)',
        link: 'Slide1',
    },
    {
        id: '1',
        image: 'images/pictures/cat-slider-2.webp',
        nickname: 'Myaso',
        description: 'Huisissl',
        quote: '',
        link: 'Slide2',
    },
    {
        id: '2',
        image: 'images/pictures/cat-slider-3.webp',
        nickname: 'Kisi',
        description: 'Huisissl',
        quote: '',
        link: 'Slide3',
    },
]