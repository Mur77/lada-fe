import { iWeekAnimal } from '../../../interfaces'

export const weekAnimals: iWeekAnimal[] = [
    {
        title: 'Собака недели',
        image: '/images/pictures/dog-week.webp',
        description: 'The dog of the week',
    },
    {
        title: 'Кошка недели',
        image: '/images/pictures/cat-week.webp',
        description: 'The cat of the week',
    },
]
