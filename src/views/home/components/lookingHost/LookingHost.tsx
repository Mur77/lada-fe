import { Img, Button } from '../../../../components'

import { messages } from '../../../../messages/messages'

import styles from './LookingHost.module.scss'

import { iAnimal } from '../../../../interfaces'

interface iLookingHost {
    animal: iAnimal
}

export const LookingHost: React.FC<iLookingHost> = ({ animal }) => {
    return (            
        <div className={styles.container}>
            <Img src={animal.image} alt={animal.nickname} className={styles.image} />
            <div className={styles.info}>
                <div className={styles.quote}><span>«</span>{animal.quote}<span>»</span></div>
                <div className={styles.nickname}>{animal.nickname}</div>
                <div className={styles.description}>{animal.description}</div>
                <div><Button type="black-red" width="120">{messages.home.more}</Button></div>
            </div>
        </div>
    )
}