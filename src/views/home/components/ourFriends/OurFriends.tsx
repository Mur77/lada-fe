import { messages } from '../../../../messages/messages'

import styles from './OurFriends.module.scss'

import { iFriend } from '../../../../interfaces'

interface iOurFriends {
    friends: iFriend[]
}

export const OurFriends: React.FC<iOurFriends> = ({ friends }) => {
    return (
        <div className={styles.container}>
            <div className={styles.title}>{messages.header.menu.ourFriends}</div>
            <div className={styles.ourFriends}>
                {friends.map((item, index) => (
                    <div className={styles.item} key={index}>
                        {item.link === '' ? (
                            <img className={styles.itemImage} src={item.image} />
                        ) : (
                            <a href={item.link} target="_blank">
                                <img className={styles.itemImage} src={item.image} />
                            </a>
                        )}
                    </div>
                ))}
            </div>
        </div>
    )
}