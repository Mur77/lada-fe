import styles from './Intro.module.scss'

import { iIntro } from '../../../../interfaces'

export const Intro: React.FC<iIntro> = ({ text }) => {
    return (
        <div className={styles.container}>
            {text}
        </div>
    )
}
