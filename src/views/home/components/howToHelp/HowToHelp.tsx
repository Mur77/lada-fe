import { messages } from '../../../../messages/messages'

import styles from './HowToHelp.module.scss'

import AnimalCare from '../../../../../public/icons/how-to-help/animal-care.svg'
import Donation from '../../../../../public/icons/how-to-help/donation.svg'
import Kennel from '../../../../../public/icons/how-to-help/kennel.svg'
import Participate from '../../../../../public/icons/how-to-help/participate.svg'
import Wrench from '../../../../../public/icons/how-to-help/wrench.svg'

export const HowToHelp = () => {
    return (
        <div className={styles.container}>
            <div className={styles.title}>{`${messages.header.menu.howToHelp}?`}</div>
            <div className={styles.howToHelp}>
                <div className={styles.item}>
                    <Participate />
                    <div className={styles.itemText}>{messages.header.submenu5.pos1}</div>
                </div>
                <a className={styles.item}>
                    <Donation />
                    <div className={styles.itemText}>{messages.header.submenu5.pos2}</div>
                </a>
                <a className={styles.item}>
                    <Wrench />
                    <div className={styles.itemText}>{messages.header.submenu5.pos3}</div>
                </a>
                <a className={styles.item}>
                    <AnimalCare />
                    <div className={styles.itemText}>{messages.header.submenu5.pos4}</div>
                </a>
                <a className={styles.item}>
                    <Kennel />
                    <div className={styles.itemText}>{messages.header.submenu5.pos5}</div>
                </a>
            </div>
        </div>
    )
}