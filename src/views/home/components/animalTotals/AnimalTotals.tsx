import { messages } from '../../../../messages/messages'

import styles from './AnimalTotals.module.scss'

interface iAnimalTotals {
    title: string
    leftIcon: any
    rightIcon: any
    leftAmount: number
    rightAmount: number
}

export const AnimalTotals: React.FC<iAnimalTotals> = ({
    title, leftIcon, rightIcon, leftAmount, rightAmount
}) => {
    return (
        <>
            <div className={styles.title}>{title}</div>
            <div className={styles.allAnimals}>
                <div className={styles.animalsData}>
                    <div className={styles.animalIcon}>{leftIcon()}</div>
                    <div className={styles.animalAmount}>{leftAmount}</div>
                </div>
                <div className={styles.animalsData}>
                    <div className={styles.animalIcon}>{rightIcon()}</div>
                    <div className={styles.animalAmount}>{rightAmount}</div>
                </div>
            </div>
        </>
    )
}