import { Img } from '../../../../components/img/Img'

import styles from './WeekAnimal.module.scss'

import { iWeekAnimal } from '../../../../interfaces'

export const WeekAnimal: React.FC<iWeekAnimal> = ({ title, description, image }) => {
    const placeholder = '../../../../../public/icons/paw.svg'

    return (
        <div className={styles.container}>
            <Img className={styles.image} src={image} alt={title} placeholderPath={placeholder} />
            <div className={styles.textContainer}>
                <div className={styles.title}>{title}</div>
                <div className={styles.description}>{description}</div>
            </div>
        </div>
    )
}