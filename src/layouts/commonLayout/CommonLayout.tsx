import { Header, Footer } from '../../components'

import styles from './CommonLayout.module.scss'

export const CommonLayout = ({ children }) => {
    return (
        <>
            <div className={styles.container}>
                <Header />
                {children}
            </div>
            <Footer />
        </>
    )
}