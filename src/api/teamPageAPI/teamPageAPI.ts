import { axiosInstance } from '../axiosInstance'
import { iUser } from '../../interfaces'

export const teamPageAPI = {
    async getTeamMembers(): Promise<iUser[]> {
        const { data } = await axiosInstance.get('/team/members')

        return data.results
    },
}
