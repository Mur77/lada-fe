import { useQuery, UseQueryResult } from 'react-query'
import { GET_TEAM_MEMBERS } from '../queries'
import { teamPageAPI } from '../teamPageAPI/teamPageAPI'
import { iUser } from '../../interfaces'

export const useTeamMembers = (): UseQueryResult<iUser[], unknown> =>
    useQuery(GET_TEAM_MEMBERS, teamPageAPI.getTeamMembers)
