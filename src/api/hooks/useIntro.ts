import { useQuery, UseQueryResult } from 'react-query'
import { GET_INTRO } from '../queries'
import { homePageAPI } from '../homePageAPI/homePageAPI'
import { iIntro } from '../../interfaces'

export const useNews = (
    limit: number,
    initialData: iIntro
): UseQueryResult<iIntro, unknown> => useQuery(GET_INTRO, () => homePageAPI.getIntro(), { initialData })

