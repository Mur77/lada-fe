import { useQuery, UseQueryResult } from 'react-query'
import { GET_SLIDER_IMAGES } from '../queries'
import { iAnimal, iHomeSlide } from '../../interfaces'
import { homePageAPI } from '../homePageAPI/homePageAPI'

export const useHomePageGallery = (
    initialData: iHomeSlide[]
): UseQueryResult<iHomeSlide[], unknown> =>
    useQuery(
        [GET_SLIDER_IMAGES], () => homePageAPI.getSliderImages(), { initialData, }
    )
