import { useQuery, UseQueryResult } from 'react-query'
import { GET_LOOKING_RANDOM } from '../queries'
import { homePageAPI } from '../homePageAPI/homePageAPI'
import { iAnimal } from '../../interfaces'

export const useIMVideos = (
    initialData: iAnimal
): UseQueryResult<iAnimal, unknown> =>
    useQuery(GET_LOOKING_RANDOM, () => homePageAPI.getLookingRandom(), {
        initialData,
    })
