import { useQuery, UseQueryResult } from 'react-query'
import { GET_WEEK_ANIMAL } from '../queries'
import { iAnimal } from '../../interfaces'
import { homePageAPI } from '../homePageAPI/homePageAPI'

export const useHomePageGallery = (
    initialData: iAnimal[]
): UseQueryResult<iAnimal[], unknown> =>
    useQuery(
        [GET_WEEK_ANIMAL], () => homePageAPI.getWeekAnimal(), { initialData, }
    )
