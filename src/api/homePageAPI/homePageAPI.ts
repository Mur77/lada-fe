import { axiosInstance } from '../axiosInstance'
import {
    iHomeSlide,
    iAnimal,
    iIntro,
} from '../../interfaces'

export const homePageAPI = {
    async getSliderImages(): Promise<iHomeSlide[]> {
        const { data } = await axiosInstance.get('/main/slider')

        return data.results
    },

    async getWeekAnimal(): Promise<iAnimal[]> {
        const { data } = await axiosInstance.get('/main/week')

        return data.results
    },

    async getLookingRandom(): Promise<iAnimal> {
        const { data } = await axiosInstance.get('/main/looking')

        return data.results
    },

    async getIntro(): Promise<iIntro> {
        const { data } = await axiosInstance.get('/main/intro')

        return data.results
    },
}
