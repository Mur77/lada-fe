// Homepage
export const GET_SLIDER_IMAGES = 'GET_SLIDER_IMAGES'
export const GET_WEEK_ANIMAL = 'GET_WEEK_ANIMAL'
export const GET_LOOKING_RANDOM = 'GET_LOOKING_RANDOM'
export const GET_INTRO = 'GET_INTRO'

// Settings
export const GET_SETTINGS = 'GET_SETTINGS'

// Team page
export const GET_TEAM_MEMBERS = 'GET_TEAM_MEMBERS'
