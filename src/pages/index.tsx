import Head from 'next/head'

import { CommonLayout } from '../layouts/commonLayout/CommonLayout'
import { Home } from '../views/home/Home'

const HomePage = () => {
    return (
        <CommonLayout>
            <Home />
        </CommonLayout>
    )
}

export default HomePage
