import { useTeamMembers } from '../api/hooks/useTeamMembers'
import { CommonLayout } from '../layouts/commonLayout/CommonLayout'
import { Team } from '../views/team/Team'

const TeamPage = () => {
    // const teamMembers = useTeamMembers()

    return (
        <CommonLayout>
            <Team />
        </CommonLayout>
    )
}

export default TeamPage