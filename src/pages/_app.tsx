import React from 'react'
import { AppProps } from 'next/app'

import '../styles/globals.scss'
import 'swiper/swiper.scss'
import 'swiper/components/navigation/navigation.scss'
import 'swiper/components/pagination/pagination.scss'

function Lada({ Component, pageProps }: AppProps): JSX.Element {
    return (
        <Component {...pageProps} />
    )
}

export default Lada
