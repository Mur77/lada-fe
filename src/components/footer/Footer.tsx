import { messages } from '../../messages/messages'

import Facebook from '../../../public/icons/social/facebook.svg'
import Instagram from '../../../public/icons/social/instagram.svg'

import styles from './Footer.module.scss'

export const Footer: React.FC = () => {
    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <div className={`${styles.about} ${styles.mobileMargins}`}>{messages.footer.about}</div>
                <a className={`${styles.social} ${styles.mobileMargins}`} href="https://www.facebook.com/apsLada/" target="_blank">
                    <div className={styles.socialIcon}><Facebook /></div>
                    <div className={styles.socialText}>&nbsp;{messages.footer.ourFacebook}</div>
                </a>
                <a className={`${styles.social} ${styles.mobileMargins}`} href="https://www.instagram.com/aps_lada/" target="_blank">
                    <div className={styles.socialIcon}><Instagram /></div>
                    <div className={styles.socialText}>&nbsp;&nbsp;{messages.footer.ourInstagram}</div>
                </a>
            </div>
        </div>
    )
}
