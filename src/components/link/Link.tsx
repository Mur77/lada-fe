import NextLink from 'next/link'
import React, { ReactElement } from 'react'

interface iLink {
    as?: string
    href: string
    className?: string
    children: ReactElement
    onClick?: () => void
}

export const Link: React.FC<iLink> = ({ as = '', href, className, children, onClick }) => {
    return (
        <NextLink as={as} href={href}>
            <a onClick={onClick} className={className}>
                {children}
            </a>
        </NextLink>
    )
}
