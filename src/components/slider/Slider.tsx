import { useState, useRef, useEffect, ReactElement } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { Pagination, Autoplay, SwiperOptions } from 'swiper'
import clsx from 'clsx'

import styles from './Slider.module.scss'

import { iHomeSlide, iAnimal } from '../../interfaces'
import { url } from 'inspector'

type slideHeights = 'none' | 'height160'
type colors = 'none' | 'white'
interface iSlider {
    slides: iHomeSlide[] | iAnimal[]
    slideHeight?: slideHeights
    backgroundColor?: colors
    spaceBetween?: number
    enablePagination?: boolean
    slidesPerView?: number | 'auto'
    centerSlides?: boolean
    breakpoints?: {
        // when window width is >= 576px
        576: {
            slidesPerView: number
            spaceBetween: number
        }
        // when window width is >= 768px
        768: {
            slidesPerView: number
            spaceBetween: number
        }
        // when window width is >= 992px
        992: {
            slidesPerView: number
            spaceBetween: number
        }
        // when window width is >= 1200px
        1200: {
            slidesPerView: number
            spaceBetween: number
        }
}
}

SwiperCore.use([Pagination, Autoplay])

export const Slider: React.FC<iSlider> = ({ 
    slides, 
    slideHeight='none', 
    backgroundColor='none',
    enablePagination=true,
    slidesPerView='auto',
    spaceBetween=30,
    centerSlides=true,
}) => {
    const [paginationContainer, setPaginationContainer] = useState(null)
    const [prevElem, setPrevElem] = useState(null)
    const [nextElem, setNextElem] = useState(null)

    const paginationRef = useRef(null)
    const prevRef = useRef(null)
    const nextRef = useRef(null)

    const params: SwiperOptions = {
        spaceBetween: spaceBetween,
        centeredSlides: centerSlides,
        loop: true,
        slidesPerView: slidesPerView,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
            pauseOnMouseEnter: true,
        },
    }

    if (enablePagination) {
        params.pagination = {
            type: 'bullets',
            el: paginationContainer,
            clickable: true,
            bulletClass: styles.paginationBullet,
            bulletActiveClass: styles.paginationBulletActive,
            bulletElement: 'div',
        }
    }

    useEffect(() => {
        setPaginationContainer(paginationRef.current)
    })

    const SwiperComponent: ReactElement = (
        <>
            {paginationContainer && (
                <Swiper {...params}>
                    {slides.map((item) => (
                        <SwiperSlide key={item.id}>
                            <div
                                className={clsx(styles.slideContainer, {
                                    [styles.height160]: slideHeight === 'height160',
                                })}
                                key={item.id} 
                            >
                                <img className={styles.slideImage} src={item.image} />
                                {/* <div className={styles.slideText}>
                                    {item.text}
                                </div> */}
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>            
            )}
        </>
    )

    return (
        <>
            <div className={clsx(styles.swiperContainer, {
                [styles.swiperBackgroundWhite]: backgroundColor === 'white',
            })}>
                {SwiperComponent}
            </div>
            <div className={styles.paginationContainer}>
                <div ref={paginationRef} className={styles.paginationDots} />
            </div>
        </>
    )
}