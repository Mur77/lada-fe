import { Header } from './header/Header'
import { Slider } from './slider/Slider'
import { Footer } from './footer/Footer'
import { Img } from './img/Img'
import { Button } from './button/Button'
import { Link } from './link/Link'

export { Header, Slider, Footer, Img, Button, Link }