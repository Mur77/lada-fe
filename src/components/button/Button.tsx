import { ReactNode } from 'react'
import clsx from 'clsx'

import styles from './Button.module.scss'

interface iButton {
    children: ReactNode
    type?: 'red' | 'white' | 'white-red' | 'black-red'
    disabled?: boolean
    submit?: boolean
    loading?: boolean
    block?: boolean
    width?: string
    onClick?: () => void
    bigButton?: boolean
}

export const Button: React.FC<iButton> = ({
    loading,
    children,
    type = 'red',
    disabled,
    submit,
    block,
    width,
    onClick = () => {},
    bigButton,
}) => {
    const isDisabled = disabled || loading

    return (
        <button
            type={submit ? 'submit' : 'button'}
            className={clsx(styles[type], {
                [styles.disabled]: isDisabled,
                [styles.block]: block,
                [styles.bigButton]: bigButton,
            })}
            style={width ? { width: `${width}px` } : {}}
            onClick={onClick}
        >
            {children}
        </button>
    )
}