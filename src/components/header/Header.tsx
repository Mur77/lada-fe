import MediaQuery from 'react-responsive'
import { MenuDesktop } from './components/menu/menuDesktop/MenuDesktop'
import { MenuMobile } from './components/menu/menuMobile/MenuMobile'
import { messages } from '../../messages/messages'

import { XL } from '../../constants'

import styles from './Header.module.scss'

export const Header: React.FC = () => {
    return (
        <div className={styles.container}>
            <div className={styles.logo}>
                <img className={styles.logoImage} src='images/lada-logo-small.png' alt='lada-logo' />
                <span className={styles.logoText}>{messages.header.logoText}</span>
            </div>
            <MediaQuery minWidth={XL+1}>
                <MenuDesktop />
            </MediaQuery>
            <MediaQuery maxWidth={XL}>
                <MenuMobile />
            </MediaQuery>
        </div>
    )
}