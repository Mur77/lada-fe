import { useState, useEffect, useRef } from 'react'
import clsx from 'clsx'

import { Link } from '../../../..'
import { menuItems } from '../menuItems/menuItems'

import Burger from '../../../../../../public/icons/burger.svg'

import styles from './MenuMobile.module.scss'

export const MenuMobile = () => {
    const [submenus, setSubmenus] = useState(Array(menuItems.length).fill(false))
    const [menuOpen, setMenuOpen] = useState(false)
    const menuContainer = useRef(null)

    useEffect(() => {
        document.body.addEventListener('click', handleClickOutside);

        return (() => {
            document.body.removeEventListener('click', handleClickOutside);
        })
    })

    const handleBurgerClick = () => {
        setMenuOpen(!menuOpen)
        // Если закрываем меню, то закрываем и открытое подменю
        if (menuOpen === true) {
            setSubmenus(Array(menuItems.length).fill(false))
        }
    }

    const handleItemClick = (menuID: number) => {
        setSubmenus((prevState) => {
            const state = Array(menuItems.length).fill(false)
            state[menuID] = !prevState[menuID]
            return state
        })
    }

    const handleMenuContainerClick = (e) => {
        // Предотвращаем всплытие события click за пределы меню
        e.stopPropagation();
    }

    const handleClickOutside = () => {
        // При клике вне контейнера меню закрываем его
        setMenuOpen(false)
        setSubmenus(Array(menuItems.length).fill(false))
    }

    return (
        <div ref={menuContainer} className={styles.container} onClick={handleMenuContainerClick}>
            <Burger
                className={styles.burger}
                onClick={handleBurgerClick}
            />
            <div className={clsx(styles.verticalMenu, {
                [ styles.menuOpen ]: menuOpen,
                [ styles.menuClosed ]: !menuOpen,
            })}>
                {menuItems.map( (item, index) => (
                    <Link href={item.link !== '' && item.submenu.length === 0 ? item.link : ''}>
                        <div key={index} className={clsx(styles.menuItem, {
                            [ styles.menuItemSelected ]: submenus[index],
                        })}
                            onClick={item.submenu.length > 0 ? ()=>handleItemClick(index) : () => {}}
                        >
                            {item.title}
                            {item.submenu.length > 0 && (
                                <div className={styles.submenuWrapper}>
                                    <div className={clsx(styles.submenu, {
                                        [ styles.menuOpen ]: submenus[index], 
                                        [ styles.menuClosed ]: !submenus[index],
                                    })}>
                                        {item.submenu.map( (item, index) => (
                                            <Link href={item.link ? item.link : '/'}>
                                                <div key={index} className={styles.submenuItem}>{item.title}</div>
                                            </Link>
                                        ))}
                                    </div>
                                </div>
                            )}
                        </div>
                    </Link>
                ))}
            </div>
        </div>
    )
}
