import { messages } from '../../../../../messages/messages'

import { iMenuItem } from '../../../../../interfaces'

const msg = messages.header
export const menuItems: iMenuItem[] = [
    {
        title: msg.menu.main,
        link: '/index',
        submenu: [],
    },
    {
        title: msg.menu.about,
        link: '',
        submenu: [
            {
                title: msg.submenu2.pos1,
                link: '/team',
            },
            {
                title: msg.submenu2.pos2,
                link: '',
            },
            {
                title: msg.submenu2.pos3,
                link: '',
            },
        ],
    },
    {
        title: msg.menu.lookHome,
        link: '',
        submenu: [
            {
                title: msg.submenu3.pos1,
                link: '',
            },
            {
                title: msg.submenu3.pos2,
                link: '',
            },
        ],
    },
    {
        title: msg.menu.ourAnimals,
        link: '',
        submenu: [
            {
                title: msg.submenu4.pos1,
                link: '',
            },
            {
                title: msg.submenu4.pos2,
                link: '',
            },
        ],
    },
    {
        title: msg.menu.howToHelp,
        link: '',
        submenu: [],
        // submenu: [
        //     {
        //         title: msg.submenu5.pos1,
        //         link: '',
        //     },
        //     {
        //         title: msg.submenu5.pos2,
        //         link: '',
        //     },
        //     {
        //         title: msg.submenu5.pos3,
        //         link: '',
        //     },
        //     {
        //         title: msg.submenu5.pos4,
        //         link: '',
        //     },
        //     {
        //         title: msg.submenu5.pos5,
        //         link: '',
        //     },
        // ],
    },
    {
        title: msg.menu.contacts,
        link: '',
        submenu: [],
    },
]