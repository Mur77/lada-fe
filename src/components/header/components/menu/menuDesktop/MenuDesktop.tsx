import { useState } from 'react'
import clsx from 'clsx'

import { Link } from '../../../..'
import { menuItems } from '../menuItems/menuItems'

import styles from './MenuDesktop.module.scss'

export const MenuDesktop = () => {
    const [submenus, setSubmenus] = useState(Array(menuItems.length).fill(false))

    const handleHover = (menuID: number) => {
        setSubmenus((prevState) => {
            const state = Object.assign([], prevState)
            state[menuID] = true
            return state
        })
    }

    const handleLeave = (menuID: number) => {
        setSubmenus((prevState) => {
            const state = Object.assign([], prevState)
            state[menuID] = false
            return state
        })
    }

    return (
        <div className={styles.container}>
            {menuItems.map( (item, index) => (
                <Link href={item.link !== '' && item.submenu.length === 0 ? item.link : ''}>
                    <div key={index} className={styles.menuItem}
                        onMouseEnter={item.submenu.length > 0 ? ()=>handleHover(index) : () => {}}
                        onMouseLeave={item.submenu.length > 0 ? ()=>handleLeave(index) : () => {}}
                    >
                        {item.title}
                        <div className={styles.separator}></div>
                        {item.submenu.length > 0 && (
                            <div className={styles.submenuWrapper}>
                                <div className={clsx(styles.submenu, {
                                    [ styles.submenuDisplay ]: submenus[index], 
                                    [ styles.submenuNodisplay ]: !submenus[index],
                                })}>
                                    {item.submenu.map( (item, index) => (
                                        <Link href={item.link ? item.link : '/'}>
                                            <div key={index} className={styles.submenuItem}>{item.title}</div>
                                        </Link>
                                    ))}
                                </div>
                            </div>
                        )}
                    </div>
                </Link>
            ))}
        </div>
    )
}
