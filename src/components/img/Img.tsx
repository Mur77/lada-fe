/**
 * Компонент проверки доступности изображений.
 * Выводит заглушку, если картинка еще не загружена или недоступна.
 * Когда картинка становится доступной, выводит ее.
 */

import React, { useState, useEffect } from 'react'

interface iImg {
    src: string
    alt: string
    placeholderPath?: string
    className?: string
    width?: number
    height?: number
    onClick?: () => void
}

export const Img: React.FC<iImg> = ({
    src,
    alt,
    placeholderPath = '/images/placeholder/imgPlaceholder.png',
    className = '',
    onClick = () => {},
}) => {
    const [imageReady, setImageReady] = useState(false)

    useEffect(() => {
        // Проверяем, успешно ли загружается картинка
        const image = new Image()

        image.src = src
        image.onload = () => setImageReady(true)

        return () => {
            image.onload = null
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <img
            src={imageReady ? src : placeholderPath}
            alt={alt}
            className={className}
            onClick={onClick}
        />
    )
}
